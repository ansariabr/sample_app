require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest

  def setup
    @user = users(:michael)
  end

  test "layout links" do
    get root_path
    assert_template 'static_pages/home'
    assert_select "a[href=?]", root_path, count: 2
    assert_select "a[href=?]", help_path
    assert_select "a[href=?]", about_path
    assert_select "a[href=?]", contact_path
    get contact_path
    assert_select "title", full_title("Contact")
    get signup_path
    assert_select "title", full_title("Sign up")
    #check for redirection in case of non-logged in users
    get users_path
    assert_redirected_to login_url
    #verify count in case of logged in users
    log_in_as(@user)
    get users_path
    assert_select "title", full_title("All users")
    # assert_select "div.container ul.users li", User.count
  end

end
